use cell;
use lattice2d::Lattice2d;
use multi_lattice2d::MultiLattice2d;
use lattice_ops_2d::MultiLatticeOps2d;
use box2d::Box2d;
use lbm_hlp2d::{symmetrize,strain_to_stress};
use fd_hlp2d;
use ind_hlp;
use dynamics::Dynamics;
use rayon::prelude::*;
use core::marker::Sync;

/// ProcFun trait contains the methods for processing functionals
pub trait ProcFunMl2d : ProcFunMl2dInterface + Sync {
	/// Executes in parallel the processing functional and communicates if necessary
	fn process(&self, ml: &mut MultiLattice2d) {
		self.get_app_domains().par_iter().zip(ml.get_mut_lattices().par_iter_mut()).for_each(
			|(b,l)| {
				match *b {
					Some(b) => {
						self.process_atomic(l, &b);
					},
					None => {},
				}
		});

		if *self.do_communcation() {
			ml.communicate();
		}
	}

	/// Atomic lattice execution of the processing functional
	/// on a Lattice2d
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d);
}

/// ProcFun trait contains the methods for processing functionals
pub trait ProcFunMl2dInterface {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>>;

	fn do_communcation<'a>(&'a self) -> &'a bool;
}

/// Boundary condition functional for flat walls
#[derive(Debug)]
pub struct FlatInterpDirichlet2d {
	dir: i32,
	ori: i32,
	app_domains: Vec<Option<Box2d>>,
	communicate: bool,
}

impl FlatInterpDirichlet2d  {
	pub fn new(dir: i32, ori: i32, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// -ml.get_env() is needed because of the non locality of the algorithm
			app_domains.push(dom.translate(-loc).intersection(bb.enlarge(-(ml.get_env() as i32))));
		}

		// A communication is needed on the populations because of the non-locallity of the algorithm
		FlatInterpDirichlet2d{dir:dir, ori:ori, app_domains: app_domains, communicate: true}
	}
}

impl ProcFunMl2dInterface for FlatInterpDirichlet2d {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}

	fn do_communcation<'a>(&'a self) -> &'a bool {
		&self.communicate
	}
}

impl ProcFunMl2d for FlatInterpDirichlet2d {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) {
		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 

				let (rho, u, pi) = {
					let cell  = lat.get_cell(ix as usize, iy as usize);

					let rho   = cell.compute_rho();
					let u     = cell.compute_u();
					let omega = cell.get_rel_freq();

					let grad_u = [fd_hlp2d::o1_vel_derivative(lat, ix, iy, 0, self.ori, self.dir != 0), 
					              fd_hlp2d::o1_vel_derivative(lat, ix, iy, 1, self.ori, self.dir != 1)];
					let pi = strain_to_stress(&symmetrize(&grad_u), &rho, &omega);

					(rho, u, pi)
		        };

		        lat.get_mut_cell(i).regularize(&rho, &u, &pi);
			}
		}
	}

}


/// Boundary condition functional for corners
#[derive(Debug)]
pub struct CornerInterpDirichlet2d {
	x_norm: i32,
	y_norm: i32,
	app_domains: Vec<Option<Box2d>>,
	communicate: bool,
}

impl CornerInterpDirichlet2d  {
	pub fn new(x_norm: i32, y_norm: i32, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// -ml.get_env() is needed because of the non locality of the algorithm
			app_domains.push(dom.translate(-loc).intersection(bb.enlarge(-(ml.get_env() as i32))));
		}

		// A communication is needed on the populations because of the non-locallity of the algorithm
		CornerInterpDirichlet2d{x_norm:x_norm, y_norm:y_norm, app_domains: app_domains, communicate: true}
	}
}

impl ProcFunMl2dInterface for CornerInterpDirichlet2d {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}

	fn do_communcation<'a>(&'a self) -> &'a bool {
		&self.communicate
	}
}

impl ProcFunMl2d for CornerInterpDirichlet2d {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) {
		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 

				let (rho, u, pi) = {
					let (x_px, y_px) = ind_hlp::get_next(ix, iy, 0, -self.x_norm);
					let (x_py, y_py) = ind_hlp::get_next(ix, iy, 1, -self.y_norm);

					let cell = lat.get_cell(ix as usize, iy as usize);

					let rho  = 0.5f64*(lat.get_cell(x_px as usize, y_px as usize).compute_rho() 
									 + lat.get_cell(x_py as usize, y_py as usize).compute_rho());

					let u = cell.compute_u();
					let omega = cell.get_rel_freq();

					let grad_u = [fd_hlp2d::o1_vel_derivative(lat, ix, iy, 0, self.x_norm, false), 
					              fd_hlp2d::o1_vel_derivative(lat, ix, iy, 1, self.y_norm, false)];
					let pi = strain_to_stress(&symmetrize(&grad_u), &rho, &omega);

					(rho, u, pi)
		        };

				lat.get_mut_cell(i).set_rho(&rho);

		        lat.get_mut_cell(i).regularize(&rho, &u, &pi);
			}
		}
	}

}



#[derive(Debug)]
pub struct SetDyn2d<V: Dynamics+Sync+Clone> {
	dyn: V,
	app_domains: Vec<Option<Box2d>>,
	communicate: bool,
}

impl <V: Dynamics+Sync+Clone> SetDyn2d<V> {
	pub fn new(dyn: V, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// No enlarge is needed because of the non locality of the algorithm (applies also on the envelope)
			app_domains.push(dom.translate(-loc).intersection(bb));
		}

		// No communication is needed on the populations because of the locallity of the algorithm
		SetDyn2d{dyn: dyn, app_domains: app_domains, communicate: false}
	}
}

impl <V: Dynamics+Sync+Clone> ProcFunMl2dInterface for SetDyn2d<V> {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}

	fn do_communcation<'a>(&'a self) -> &'a bool {
		&self.communicate
	}
}

impl <V: Dynamics+Sync+Clone> ProcFunMl2d for SetDyn2d<V> {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) {
		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 
				lat.set_dyn(i, self.dyn.clone());
			}
		}
	}

}


#[derive(Debug)]
pub struct SetRho2d<V> where V:Sync+Fn(i32,i32) -> f64
{
	app_domains: Vec<Option<Box2d>>,
	communicate: bool,
	foo: V,
}

impl <V:Sync+Fn(i32,i32) -> f64> SetRho2d<V> {
	pub fn new(foo: V, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// No enlarge is needed because of the non locality of the algorithm (applies also on the envelope)
			app_domains.push(dom.translate(-loc).intersection(bb));
		}

		// No communication is needed on the populations because of the locallity of the algorithm
		SetRho2d{app_domains: app_domains, communicate: false, foo: foo}
	}
}

impl <V:Sync+Fn(i32,i32) -> f64> ProcFunMl2dInterface for SetRho2d<V> {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}

	fn do_communcation<'a>(&'a self) -> &'a bool {
		&self.communicate
	}
}

impl <V:Sync+Fn(i32,i32) -> f64> ProcFunMl2d for SetRho2d<V> {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) {
		let loc = lat.get_loc().clone();

		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 
				let rho = (self.foo)(ix+loc.x, iy+loc.y);
				lat.get_mut_cell(i).set_rho(&rho);
			}
		}
	}

}


#[derive(Debug)]
pub struct SetVel2d<V> where V:Sync+Fn(i32,i32) -> [f64; cell::D]
{
	app_domains: Vec<Option<Box2d>>,
	communicate: bool,
	foo: V,
}

impl <V:Sync+Fn(i32,i32) -> [f64; cell::D]> SetVel2d<V> {
	pub fn new(foo: V, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// No enlarge is needed because of the non locality of the algorithm (applies also on the envelope)
			app_domains.push(dom.translate(-loc).intersection(bb));
		}

		// No communication is needed on the populations because of the locallity of the algorithm
		SetVel2d{app_domains: app_domains, communicate: false, foo: foo}
	}
}

impl <V:Sync+Fn(i32,i32) -> [f64; cell::D]> ProcFunMl2dInterface for SetVel2d<V> {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}

	fn do_communcation<'a>(&'a self) -> &'a bool {
		&self.communicate
	}
}

impl <V:Sync+Fn(i32,i32) -> [f64; cell::D]> ProcFunMl2d for SetVel2d<V> {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) {
		let loc = lat.get_loc().clone();

		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 
				let u = (self.foo)(ix+loc.x, iy+loc.y);
				lat.get_mut_cell(i).set_u(&u);
			}
		}
	}

}

#[derive(Debug)]
pub struct SetDevStress2d<V> where V:Sync+Fn(i32,i32) -> [[f64; cell::D]; cell::D]
{
	app_domains: Vec<Option<Box2d>>,
	communicate: bool,
	foo: V,
}

impl <V:Sync+Fn(i32,i32) -> [[f64; cell::D]; cell::D]> SetDevStress2d<V> {
	pub fn new(foo: V, ml: &MultiLattice2d, dom: Box2d) -> Self {
		let mut app_domains = Vec::new();
		for &(bb, loc) in ml.get_multi_block().get_boxes_offsets().iter() {
			// No enlarge is needed because of the non locality of the algorithm (applies also on the envelope)
			app_domains.push(dom.translate(-loc).intersection(bb));
		}

		// No communication is needed on the populations because of the locallity of the algorithm
		SetDevStress2d{app_domains: app_domains, communicate: false, foo: foo}
	}
}

impl <V:Sync+Fn(i32,i32) -> [[f64; cell::D]; cell::D]> ProcFunMl2dInterface for SetDevStress2d<V> {
	fn get_app_domains<'a>(&'a self) -> &'a Vec<Option<Box2d>> {
		&self.app_domains
	}

	fn do_communcation<'a>(&'a self) -> &'a bool {
		&self.communicate
	}
}

impl <V:Sync+Fn(i32,i32) -> [[f64; cell::D]; cell::D]> ProcFunMl2d for SetDevStress2d<V> {
	fn process_atomic(&self, lat: &mut Lattice2d, dom: &Box2d) {
		let loc = lat.get_loc().clone();

		for ix in dom.x0..dom.x1 {
			for iy in dom.y0..dom.y1 {
				let i = ind_hlp::get_grid_idx(ix as usize, iy as usize, lat.get_nx(), lat.get_ny()); 
				let pi = (self.foo)(ix+loc.x, iy+loc.y);
				lat.get_mut_cell(i).set_deviatoric_stress(&pi);
			}
		}
	}

}

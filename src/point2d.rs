use std::ops::Add;
use std::ops::Sub;
use std::ops::Neg;

use box2d::Box2d;


struct Point2dBuilder {
    x: i32,
    y: i32,
}

impl Point2dBuilder {
    fn new() -> Self{
        Point2dBuilder{x: 0, y: 0}
    }

    fn set_x(&mut self, x_:i32) -> &mut Self{
        self.x = x_;
        self
    }

    fn set_y(&mut self, y_:i32) -> &mut Self{
        self.y = y_;
        self
    }

    fn finalize(&mut self) -> Point2d {
        Point2d{ x: self.x, y: self.y, }
    }
}

/// Point structure in 2 dimensions on an evenly integer spaced grid. 
#[derive(Debug,Clone,Copy)]
pub struct Point2d
{
    pub x: i32,
    pub y: i32,
}

impl Point2d {
    pub fn new(x: i32, y: i32) -> Point2d {
        Point2dBuilder::new().set_x(x).set_y(y).finalize()
    }

    /// This function returns true if the point is contained in b ( a 2d box)
    pub fn is_contained(&self, b: Box2d) -> bool {
        (self.x >= b.x0 && self.x <= b.x1 ) 
            && (self.y >= b.y0 && self.y <= b.y1 ) 
    }
}

/// The addition of a Point2d with another Point2d
/// 
/// # Arguments
///
/// * `&self` - A Point2d
/// * `rhs`  - A Point2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// fn main () {
///     let p1 = Point2d::new(1, 4);
///     let p2 = Point2d::new(3, 8);
///     
///     assert_eq!(p1+p2, Point2d::new(4, 12));
/// }
/// ```
impl Add for Point2d {
    type Output = Point2d;

    fn add(self, rhs: Point2d) -> Point2d {
        Point2d{ x: self.x + rhs.x, y: self.y + rhs.y}
    }
}

/// The subtraction of a Point2d with another Point2d
/// 
/// # Arguments
///
/// * `&self` - A Point2d
/// * `rhs`  - A Point2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// fn main () {
///     let p1 = Point2d::new(1, 4);
///     let p2 = Point2d::new(3, 8);
///     
///     assert_eq!(p1-p2, Point2d::new(-2, -4));
/// }
/// ```
impl Sub for Point2d {
    type Output = Point2d;

    fn sub(self, rhs: Point2d) -> Point2d {
        Point2d{ x: self.x - rhs.x, y: self.y - rhs.y}
    }
}

/// The unary - of a Point2d
/// 
/// # Arguments
///
/// * `&self` - A Point2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// fn main () {
///     let p1 = Point2d::new(1, 4);
///     
///     assert_eq!(-p1, Point2d::new(-1, -4));
/// }
/// ```
impl Neg for Point2d {
    type Output = Point2d;

    fn neg(self) -> Point2d {
        Point2d{ x: -self.x, y: -self.y}
    }
}


/// The equality of two Point2d 
/// 
/// # Arguments
///
/// * `&self` - A Point2d
/// * `&other`  - A Point2d
///
/// # Example
///
/// ```
/// extern crate palarust;
/// use palarust::point2d::Point2d;
/// fn main () {
///     let b = Point2d::new(1, 4);
///     
///     assert_eq!(b, Point2d::new(1, 4));
///     assert!(b != Point2d::new(1, 5), "b is not equal to Point2d::new(1, 4).");
/// }
/// ```
impl PartialEq for Point2d {
    fn eq(&self, other: &Point2d) -> bool {
        (self.x == other.x && self.y == other.y)
    }
}




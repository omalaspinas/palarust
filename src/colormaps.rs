use sdl2::pixels::Color;
use core;


pub trait ColorMap : core::fmt::Debug {
	fn get_color(&self, x: &f64) -> Result<Color, &str>;
}

#[derive(Debug)]
pub struct ShortRainbow;

impl ColorMap for ShortRainbow {
	fn get_color(&self, x: &f64) -> Result<Color, &str> {
		let a = (1.0 - x)/0.25;	//invert and group
		let b = a.floor() as u8;	//this is the integer part
		let c = (255.0*(a-b as f64)).floor() as u8; //fractional part from 0 to 255
		match b {
	        0 => Ok(Color::RGB(255u8, c, 0u8)),
	        1 => Ok(Color::RGB(255u8-c, 255u8, 0u8)),
	        2 => Ok(Color::RGB(0u8, 255u8, c)),
	        3 => Ok(Color::RGB(0u8, 255u8-c, 255u8)),
	        4 => Ok(Color::RGB(0u8, 0u8, 255u8)),
	        _ => Err("The value of b is not in {0,1,2,3,4}."),
	    }
	}
}

#[derive(Debug)]
pub struct Grayscale;

impl ColorMap for Grayscale {
	fn get_color(&self, x: &f64) -> Result<Color, &str> {
		if x >= &0.0f64 && x <= &1.0f64 {
		Ok(Color::RGB((255f64*x) as u8, (255f64*x) as u8, (255f64*x) as u8))
	} else {
		Err("The value of x is not in [0,1].")
	}
	}
}

#[derive(Debug)]
pub struct InvGrayscale;

impl ColorMap for InvGrayscale {
	fn get_color(&self, x: &f64) -> Result<Color, &str> {
		if x >= &0.0f64 && x <= &1.0f64 {
			Ok(Color::RGB((255f64*(1f64-x)) as u8, (255f64*(1f64-x)) as u8, (255f64*(1f64-x)) as u8))
		} else {
			Err("The value of x is not in [0,1].")
		}
	}
}

#[derive(Debug)]
pub struct LongRainbow;

impl ColorMap for LongRainbow {
	fn get_color(&self, x: &f64) -> Result<Color, &str> {
		let a = (1.0 - x)/0.2;	//invert and group
		let b = a.floor() as u8;	//this is the integer part
		let c = (255.0*(a-b as f64)).floor() as u8; //fractional part from 0 to 255
		match b {
	        0 => Ok(Color::RGB(255u8,   c,       0u8)),
	        1 => Ok(Color::RGB(255u8-c, 255u8,   0u8)),
	        2 => Ok(Color::RGB(0u8,     255u8,   c)),
	        3 => Ok(Color::RGB(0u8,     255u8-c, 255u8)),
	        4 => Ok(Color::RGB(c,       0u8,     255u8)),
	        5 => Ok(Color::RGB(255u8,   0u8,     255u8)),
	        _ => Err("The value of b is not in {0,1,2,3,4,5}."),
	    }
	}
}

#[derive(Debug)]
pub struct YellowRed;

impl ColorMap for YellowRed {
	fn get_color(&self, x: &f64) -> Result<Color, &str> {
		let a = 1.0 - x;	//invert and group
		let b = (a*255f64).floor() as u8;	//this is the integer part
		if a >= 0.0f64 && a <= 1.0f64 {
			Ok(Color::RGB(255u8, b, 0u8))
		} else {
			Err("The value of x is not in [0,1].")
		}
	}
}

// /*plot short rainbow RGB*/
// pub fn short_rainbow(x: &f64) -> Result<Color, &str> {
// 	let a = (1.0 - x)/0.25;	//invert and group
// 	let b = a.floor() as u8;	//this is the integer part
// 	let c = (255.0*(a-b as f64)).floor() as u8; //fractional part from 0 to 255
// 	match b {
//         0 => Ok(Color::RGB(255u8, c, 0u8)),
//         1 => Ok(Color::RGB(255u8-c, 255u8, 0u8)),
//         2 => Ok(Color::RGB(0u8, 255u8, c)),
//         3 => Ok(Color::RGB(0u8, 255u8-c, 255u8)),
//         4 => Ok(Color::RGB(0u8, 0u8, 255u8)),
//         _ => Err("The value of b is not in {0,1,2,3,4}."),
//     }
// }

// /*plot grayscale RGB*/
// pub fn grayscale(x: &f64) -> Result<Color, &str> {

// 	if x >= &0.0f64 && x <= &1.0f64 {
// 		Ok(Color::RGB((255f64*x) as u8, (255f64*x) as u8, (255f64*x) as u8))
// 	} else {
// 		Err("The value of x is not in [0,1].")
// 	}
// }

// /*plot inv_grayscale RGB*/
// pub fn inv_grayscale(x: &f64) -> Result<Color, &str> {

// 	if x >= &0.0f64 && x <= &1.0f64 {
// 		Ok(Color::RGB((255f64*(1f64-x)) as u8, (255f64*(1f64-x)) as u8, (255f64*(1f64-x)) as u8))
// 	} else {
// 		Err("The value of x is not in [0,1].")
// 	}
// }

// /*plot long_rainbow RGB*/
// pub fn long_rainbow(x: &f64) -> Result<Color, &str> {

// 	let a = (1.0 - x)/0.2;	//invert and group
// 	let b = a.floor() as u8;	//this is the integer part
// 	let c = (255.0*(a-b as f64)).floor() as u8; //fractional part from 0 to 255
// 	match b {
//         0 => Ok(Color::RGB(255u8,   c,       0u8)),
//         1 => Ok(Color::RGB(255u8-c, 255u8,   0u8)),
//         2 => Ok(Color::RGB(0u8,     255u8,   c)),
//         3 => Ok(Color::RGB(0u8,     255u8-c, 255u8)),
//         4 => Ok(Color::RGB(c,       0u8,     255u8)),
//         5 => Ok(Color::RGB(255u8,   0u8,     255u8)),
//         _ => Err("The value of b is not in {0,1,2,3,4,5}."),
//     }

// }

// /*plot yellow_red RGB*/
// pub fn yellow_red(x: &f64) -> Result<Color, &str> {

// 	let a = 1.0 - x;	//invert and group
// 	let b = (a*255f64).floor() as u8;	//this is the integer part
// 	if a >= 0.0f64 && a <= 1.0f64 {
// 		Ok(Color::RGB(255u8, b, 0u8))
// 	} else {
// 		Err("The value of x is not in [0,1].")
// 	}

// }

// /*plot test RGB*/
// pub fn test(x: &f64) -> Result<Color, &str> {

// 	let a = 1.0 - x;	//invert and group
// 	let b = (a*255f64).floor() as u8;	//this is the integer part
// 	if a >= 0.0f64 && a <= 1.0f64 {
// 		Ok(Color::RGB(255u8, 0u8, b))
// 	} else {
// 		Err("The value of x is not in [0,1].")
// 	}

// }


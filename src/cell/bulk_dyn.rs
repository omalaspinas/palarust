use moments;
use moments::Moments;
use dynamics::{Dynamics,Parameters};
use descriptor2d;
use descriptor2d::Descriptor2d;
use cell::{D,Q,CS2,INV_CS2,W};

#[derive(Debug,Clone,Copy)]
pub struct BGKbuilder(f64);

impl BGKbuilder {
    pub fn new() -> Self {
        BGKbuilder(0.0)
    }

    pub fn set_rel_freq(&mut self, omega: f64) -> &mut Self {
        self.0 = omega;
        self
    }

    pub fn finalize(&mut self) -> BGK {
        BGK(self.0)
    }

}

#[derive(Debug,Clone,Copy)]
pub struct BGK(f64);

// ======================================================================== //
// ========================BGK dynamics implementations==================== //
// ======================================================================== //
impl BGK {
    pub fn new(omega: f64) -> BGK {
        BGKbuilder::new().set_rel_freq(omega).finalize()
    }

    pub fn default() -> BGK {
        BGKbuilder::new().finalize()   
    }
}

// ============= implementation of Parameters trait for BGK ========= //
impl Parameters for BGK {
    fn get_rel_freq<'a>(&'a self) -> &'a f64 {
        &self.0
    }

    fn set_rel_freq(&mut self, omega: f64) {
        self.0 = omega;
    }
}

// ============= implementation of Moments trait for BGK ========= //

impl moments::Moments for BGK {
    /// Computes the density from the distribution function 
    /// rho = sum_i f_i
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_rho(&self, f: &[f64; Q]) -> f64 {
        f.iter().fold(0.0, |sum, &x| sum + x)
    }

    /// Computes the momentum from the distribution function 
    /// j = sum_i f_i * c_i
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_j(&self, f: &[f64; Q]) -> [f64; D] {
            [-f[1]-f[2]-f[3]+f[5]+f[6]+f[7],
               f[1]-f[3]-f[4]-f[5]+f[7]+f[8]]
    }

    /// Computes the stress tensor from the distribution function 
    /// P = sum_i f_i * (c_i-u)*(c_i-u)
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_stress(&self, f: &[f64; Q]) -> [[f64; D]; D] {
        let rho = self.compute_rho(f);
        let u = self.compute_u(f);

        let p_xx = f[1]+f[2]+f[3]+f[5]+f[6]+f[7]-rho*u[0]*u[0];
        let p_xy = -f[1]+f[3]-f[5]+f[7]-rho*u[0]*u[1];
        let p_yy = f[1]+f[3]+f[4]+f[5]+f[7]+f[8]-rho*u[1]*u[1];

         [[p_xx, p_xy], [p_xy, p_yy]]
    }

    /// Computes the deviatoric stress tensor from the distribution function 
    /// tau = sum_i f_i * (c_i-u)*(c_i-u)-rho*cs2*I
    ///
    /// # Arguments
    ///
    /// * `f` - vector of populations
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_deviatoric_stress(&self, f: &[f64; Q]) -> [[f64; D]; D] {
        let rho = self.compute_rho(f);
        let mut stress = self.compute_stress(f);
        stress[0][0] -= rho*CS2;
        stress[1][1] -= rho*CS2;
        stress
    }
}

// // ============= implementation of Dynamics trait for BGK ========= //

impl  Dynamics for BGK {
    /// Computes the equilibrium distribution vector at order 2. 
    ///
    /// # Arguments
    ///
    /// * `rho` - the density
    /// * `u` - the velocity
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_equilibria(&self, rho: &f64, u: &[f64; D]) -> [f64; Q] {
        let mut f = [0.0; Q];
        let c_u = self.c_dot(&u);
        let u_sqr = INV_CS2*0.5*(u[0]*u[0]+u[1]*u[1]);
        for (i,res) in c_u.iter().enumerate() {
            let c_u_tmp = INV_CS2 * *res;
            f[i] = W[i] * *rho*(1.0 + c_u_tmp + 0.5 * c_u_tmp*c_u_tmp-u_sqr);
        }
        f
    }

    /// Computes the non-equilibrium distribution vector at order 2. 
    ///
    /// # Arguments
    ///
    /// * `pi` - the deviatoric stress tensor
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn compute_non_equilibria(&self, pi: &[[f64; D]; D]) -> [f64;Q] {
        let mut f = [0.0; Q];
        let h2_uu = self.h2_dot(&pi);
        for (i,res) in h2_uu.iter().enumerate() {
            f[i] = (0.5*W[i]*INV_CS2*INV_CS2) * *res;
        }
        f
    }

    /// Computes the collision (in-place f^out from f^in). 
    /// Must conserve mass and momentum.
    ///
    /// # Arguments
    ///
    /// * `f` - mutable f^in state
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn collide(&self, f: &mut [f64; Q]) {
        let rho = self.compute_rho(f);
        let u = self.compute_u(f);
        let feq = self.compute_equilibria(&rho, &u);
        for i in 0..Q {
            f[i] = f[i]*(1.0 - self.get_rel_freq())+self.get_rel_freq()*feq[i];
        }
    }
}

// // ============= implementation of Descriptor2d trait for BGK ========= //

impl  descriptor2d::Descriptor2d for BGK {
    /// Returns a vector with the prudoct H^{(1)}_i : pi.
    /// H^{(1)} is defined as:
    /// H^{(1)}_[i][a] = c[i][a];
    ///
    /// # Arguments
    ///
    /// * `u` - A vector of length D
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn c_dot(self, u: &[f64; D]) -> [f64; Q] {
        [0.0, -u[0]+u[1], -u[0], -u[0]-u[1], -u[1], u[0]-u[1], u[0], u[0]+u[1], u[1]]
    }

    /// Returns a vector with the prudoct H^{(2)}_i : pi.
    /// H^{(2)} is defined as:
    /// H^{(2)}_[i][a][b] = c[i][a]*c[i][b]-cs2*kronecker(a,b);
    ///
    /// # Arguments
    ///
    /// * `pi` - A DxD tensor
    ///
    /// # Example
    ///
    /// ```
    /// ```
    fn h2_dot(self, pi: &[[f64; D]; D]) -> [f64; Q] {
        [-CS2 * (pi[0][0]+pi[1][1]), 
         (1.0-CS2)*(pi[0][0]+pi[1][1])-(pi[0][1]+pi[1][0]), 
         (1.0-CS2)*pi[0][0]-CS2*pi[1][1], 
         (1.0-CS2)*(pi[0][0]+pi[1][1])+pi[0][1]+pi[1][0], 
         (1.0-CS2)*pi[1][1]-CS2*pi[0][0], 
         (1.0-CS2)*(pi[0][0]+pi[1][1])-(pi[0][1]+pi[1][0]), 
         (1.0-CS2)*pi[0][0]-CS2*pi[1][1], 
         (1.0-CS2)*(pi[0][0]+pi[1][1])+pi[0][1]+pi[1][0], 
         (1.0-CS2)*pi[1][1]-CS2*pi[0][0]]
    }
}

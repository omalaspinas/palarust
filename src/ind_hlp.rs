use point2d::Point2d;
use cell;


/// Returns the 1st index of a population at position (x,y)
///
/// # Examples
///
/// ```
/// extern crate palarust;
/// use palarust::ind_hlp;
/// fn main () {
///     assert_eq!(ind_hlp::get_raw_grid_idx(13,7,7,32,28),2597usize );
/// }
/// ```
pub fn get_raw_grid_idx(x:usize, y:usize, q:usize, nx: usize, ny: usize) -> usize {
	assert!(y < ny, "y={:} but must be smaller than {:}.", y, ny);
	assert!(x < nx, "x={:} but must be smaller than {:}.", x, nx);

	(y + ny*x)*q
}

/// Returns the 1st index of node at position (x,y)
///
/// # Examples
///
/// ```
/// extern crate palarust;
/// use palarust::ind_hlp;
/// fn main () {
///     assert_eq!(ind_hlp::get_grid_idx(6,7,9,11),73usize );
/// }
/// ```
pub fn get_grid_idx(x:usize, y:usize, nx: usize, ny: usize) -> usize {
	assert!(y < ny, "y={:} but must be smaller than {:}.", y, ny);
	assert!(x < nx, "x={:} but must be smaller than {:}.", x, nx);

	(y + ny*x)
}

/// Returns a (x,y) tuple corresponding to the 1d index in local coordinates
///
/// # Examples
///
/// ```
/// extern crate palarust;
/// use palarust::ind_hlp;
/// fn main () {
///     assert_eq!(ind_hlp::get_grid_indices(13,7,5),(2usize,3usize) );
/// }
/// ```
pub fn get_grid_indices(i:usize, nx: usize, ny: usize) -> (usize, usize) {
	assert!(i < nx*ny, "i={:} but must be smaller than {:}.", i, nx*ny);

	(i/ny, i % ny)
}

/// Returns a (x,y) tuple corresponding to the 1d index in absolute coordinates
///
/// # Examples
///
/// ```
/// extern crate palarust;
/// use palarust::ind_hlp;
/// use palarust::point2d::Point2d;
/// fn main () {
///     assert_eq!(ind_hlp::get_abs_grid_indices(13,7,5,Point2d::new(3,4)),(2+3,3+4) );
/// }
/// ```
pub fn get_abs_grid_indices(i:usize, nx: usize, ny: usize, loc: Point2d) -> (i32, i32) {
	assert!(i < nx*ny, "i={:} but must be smaller than {:}.", i, nx*ny);

	((i/ny) as i32 + loc.x, (i % ny) as i32 + loc.y)
}

/// Returns a tuple containing the (x,y)-indices of position 
/// next position in the dir direction moved by distance dx.
///
/// # Arguments
///
/// * `ix`  - x-position
/// * `iy`  - y-position.
/// * `dir`  - = 0 for x, =1 for y
/// * `dx`  - abount to move.
///
///
/// # Examples
///
/// ```
/// extern crate palarust;
/// use palarust::ind_hlp;
/// fn main () {
///     assert_eq!(ind_hlp::get_next(0,0,0,1), (1,0) );
///     assert_eq!(ind_hlp::get_next(0,0,0,-1), (-1,0) );
///     assert_eq!(ind_hlp::get_next(0,0,1,1), (0,1) );
///     assert_eq!(ind_hlp::get_next(0,0,1,-1), (0,-1) );
/// }
/// ```
pub fn get_next(ix: i32, iy: i32, dir: i32, dx: i32) -> (i32, i32) {
	if dir == 0 {
		(ix + dx, iy) 
	} else if dir == 1 {
		(ix, iy + dx) 
	} else {
		panic!("dir must be 0 or 1 and it was {:?}", dir);
	}
}

/// Returns a vector containing the index of the velocities
/// having a component pointing towards dir, ori.
///
/// # Examples
///
/// ```
/// extern crate palarust;
/// use palarust::ind_hlp;
/// fn main () {
///     assert_eq!(ind_hlp::sub_index(0,0), vec![0usize, 4usize, 8usize] );
///     assert_eq!(ind_hlp::sub_index(1,0), vec![0usize, 2usize, 6usize] );
///     assert_eq!(ind_hlp::sub_index(0,1), vec![5usize, 6usize, 7usize] );
///     assert_eq!(ind_hlp::sub_index(0,-1), vec![1usize, 2usize, 3usize] );
///     assert_eq!(ind_hlp::sub_index(1,1), vec![1usize, 7usize, 8usize] );
///     assert_eq!(ind_hlp::sub_index(1,-1), vec![3usize, 4usize, 5usize] );
/// }
/// ```
pub fn sub_index(dir: i32, ori: i32) -> Vec<usize>{
	let mut idx = Vec::new();
	for i in 0..cell::Q {
        if cell::C[i][dir as usize] == ori {
            idx.push(i);
        }
    }
    idx
}
use box2d::Box2d;
use cell;
use std::mem;

#[derive(Debug)]
struct Communicator2dBuilder {
	from: Vec<Box2d>,
	to: Vec<Box2d>,
}

impl Communicator2dBuilder {
	fn new() -> Self {
    	Communicator2dBuilder{
    		from : Vec::with_capacity((cell::Q-1)*mem::size_of::<Box2d>()),
    		to   : Vec::with_capacity((cell::Q-1)*mem::size_of::<Box2d>()),
    	}
    }

    fn set_from_and_to(mut self, nx: usize, ny: usize, from_boxes: Vec<Box2d>, env: usize) -> Self
    {

    	self.to.push(
            Box2d::new_from_usize(0, env, ny-env, ny)); //-1, 1 corner

    	self.from.push(  
            Box2d::new_from_usize(
                from_boxes[0].x1 as usize-2*env, from_boxes[0].x1 as usize-env,    
                env,                             2*env));

    	self.to.push(
            Box2d::new_from_usize(0,    env, env, ny-env)); //-1, 0 edge
    	self.from.push(
            Box2d::new_from_usize(
                from_boxes[1].x1 as usize -2*env, from_boxes[1].x1 as usize-env,    
                env,                              from_boxes[1].y1 as usize-env));

    	self.to.push(
            Box2d::new_from_usize(0, env, 0,    env)); //-1,-1 corner
    	self.from.push(  
            Box2d::new_from_usize(
                from_boxes[2].x1 as usize-2*env, from_boxes[2].x1 as usize-env, 
                from_boxes[2].y1 as usize-2*env, from_boxes[2].y1 as usize-env));

    	self.to.push(
            Box2d::new_from_usize(env, nx-env, 0, env)); //0,-1 edge
    	self.from.push(  
            Box2d::new_from_usize(
                env,                             from_boxes[3].x1 as usize-env, 
                from_boxes[3].y1 as usize-2*env, from_boxes[3].y1 as usize-env));

    	self.to.push(
            Box2d::new_from_usize(nx-env, nx, 0, env)); //1,-1 edge
    	self.from.push(  
            Box2d::new_from_usize(
                env,                             2*env,    
                from_boxes[4].y1 as usize-2*env, from_boxes[4].y1 as usize-env));

    	self.to.push(
            Box2d::new_from_usize(nx-env, nx, env, ny-env)); //1, 0 edge
    	self.from.push(  
            Box2d::new_from_usize(
                env,   2*env,
                env, from_boxes[5].y1 as usize-env));

    	self.to.push(
            Box2d::new_from_usize(nx-env, nx, ny-env, ny)); //1, 1 edge
    	self.from.push(  
            Box2d::new_from_usize(
                env,        2*env,    
                env,        2*env));

    	self.to.push(
            Box2d::new_from_usize(env, nx-env, ny-env, ny)); //0, 1 edge
    	self.from.push(  
            Box2d::new_from_usize(
                env, from_boxes[7].x1 as usize-env, 
                env,        2*env));

        assert_eq!(self.from.len(), self.to.len());

        for (f,t) in self.from.iter().zip(self.to.iter()) {
            assert_eq!(f.size(), t.size(), "f = {:?}, t= {:?}", f, t);
        }

    	self
    }

    fn finalize(self) -> Communicator2d {
    	Communicator2d{ from: self.from, to: self.to, }
    }
}

#[derive(Debug)]
pub struct Communicator2d {
	from: Vec<Box2d>,
	to: Vec<Box2d>,
}

impl Communicator2d {
    pub fn new(nx: usize, ny: usize, from_boxes: Vec<Box2d>, env: usize) -> Self {
    	Communicator2dBuilder::new().set_from_and_to(nx, ny, from_boxes, env).finalize()
    }

    pub fn get_from<'a>(&'a self, i: usize) -> &'a Box2d {
        &self.from[i]
    }

    pub fn get_to<'a>(&'a self) -> &'a Vec<Box2d> {
        &self.to
    }
}


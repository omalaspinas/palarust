use cell;

pub trait Descriptor2d {
	fn c_dot(self, u: &[f64; cell::D]) -> [f64; cell::Q];
	fn h2_dot(self, uu: &[[f64; cell::D]; cell::D]) -> [f64; cell::Q];
}


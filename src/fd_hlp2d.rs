use ind_hlp;
use lattice2d::Lattice2d;
use cell;

fn o2_centered(u_p1: &f64, u_m1: &f64) -> f64 {
	0.5f64*(u_p1-u_m1)
}

fn o1_forward(u0: &f64, u_p1: &f64) -> f64 {
	u_p1-u0
}

fn o1_backward(u0: &f64, u_m1: &f64) -> f64 {
	o1_forward(u_m1, u0)
}

pub fn o1_vel_derivative(lat: &Lattice2d, ix: i32, iy:i32, dir:i32, ori:i32, centered: bool) -> [f64; 2]
{

	match centered {
		true => {
			let (ix_p1, iy_p1) = ind_hlp::get_next(ix, iy, dir,  1);
			let (ix_m1, iy_m1) = ind_hlp::get_next(ix, iy, dir, -1);

			let u_p1 = lat.get_cell(ix_p1 as usize, iy_p1 as usize).compute_u();
			let u_m1 = lat.get_cell(ix_m1 as usize, iy_m1 as usize).compute_u();

			let mut d_u = [0f64, 0f64];
			for id in 0..cell::D {
				d_u[id] = o2_centered(&u_p1[id], &u_m1[id]);
			}
			d_u
		}
		false => {
			let u_0 = lat.get_cell(ix as usize, iy as usize).compute_u();

			let (ix_n, iy_n) = ind_hlp::get_next(ix, iy, dir, -ori);
			let u_n = lat.get_cell(ix_n as usize, iy_n as usize).compute_u();

			let mut d_u = [0f64, 0f64];
			for id in 0..cell::D {
				d_u[id] = ori as f64 * o1_backward(&u_0[id], &u_n[id]);
			}
			d_u
		}
	}
}
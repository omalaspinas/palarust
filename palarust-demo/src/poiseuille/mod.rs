use num_cpus;
use docopt::Docopt;
use std::process;
use palarust::cell::bulk_dyn::BGK;
use palarust::cell::bc_dyn::{VelDirichletDyn, RhoVelDirichletDyn};
use palarust::cell;
use palarust::moments::Moments;
use palarust::lattice_ops_2d::LatticeOps2d;
use palarust::multi_lattice2d;
use palarust::units::Converter;

use palarust::proc_fun::{FlatInterpDirichlet2d,CornerInterpDirichlet2d,ProcFunMl2d,SetDyn2d,SetVel2d};
use palarust::red_proc_fun::{RedProcFunToScalarMl2d, RedScalarAndPosFun2d};
use palarust::box2d::Box2d;

use palarust::util::Converger;

const USAGE: &'static str = "
	Simulation of the poiseuille flow.

	Usage: 
		poiseuille [--resolution N --iterations N] 
	    poiseuille (-h | --help)


	Options:
	    -h, --help           Show this message.
	    --resolution=<N>     Use a resolution of N in the diameter of the cylinder [default: 200].
	    --iterations=<N>     Simulate for N iterations [default: 10000].
	";

#[derive(RustcDecodable)]
pub struct Args {
    flag_resolution: usize,
    flag_iterations: usize,
}


fn load_balancing(num_cpus: usize) -> (usize, usize) {
	match num_cpus {
		4 => (2,2),
		6 => (3,2),
		8 => (4,2),
		10 => (5,2),
		12 => (4,3),
		14 => (7,2),
		15 => (5,3),
		16 => (4,4),
		_ => (num_cpus,1)
	}
}

pub fn main(args: &[String]) {
    let args: Args =
        Docopt::new(USAGE)
            .and_then(|d| d.argv(args).decode())
            .unwrap_or_else(|e| e.exit());

    run(args.flag_resolution, args.flag_iterations);
}


fn geom_setup(mut multi_lattice: &mut multi_lattice2d::MultiLattice2d, units: & Converter) {
	let (nx, ny) = (multi_lattice.get_nx(), multi_lattice.get_ny());

	let b_l = Box2d::new(0,           1,         1,           ny as i32-1);
	let b_r = Box2d::new(nx as i32-1, nx as i32, 1,           ny as i32-1);
	let b_t = Box2d::new(1, nx as i32 - 1,       ny as i32-1, ny as i32);
	let b_b = Box2d::new(1, nx as i32 - 1,       0,           1);

	let b_lt = Box2d::new(0, 1, ny as i32-1, ny as i32);
	let b_lb = Box2d::new(0, 1, 0, 1);
	let b_rb = Box2d::new(nx as i32-1, nx as i32, 0, 1);
	let b_rt = Box2d::new(nx as i32-1, nx as i32, ny as i32-1, ny as i32);


	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0, -1),
		 &multi_lattice, b_l).process(&mut multi_lattice);
	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0,  1), 
		&multi_lattice, b_r).process(&mut multi_lattice);
	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 1,  1),
		 &multi_lattice, b_t).process(&mut multi_lattice);
	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 1, -1), 
		&multi_lattice, b_b).process(&mut multi_lattice);

	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()),  1,  1),
		&multi_lattice, b_lt).process(&mut multi_lattice);
	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()),  1, -1), 
		&multi_lattice, b_lb).process(&mut multi_lattice);
	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()),  1, -1),
		&multi_lattice, b_rb).process(&mut multi_lattice);
	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()),  1,  1), 
		&multi_lattice, b_rt).process(&mut multi_lattice);

	let poiseuille_vel = move |_ix: i32, iy:i32| -> [f64; cell::D] {
		let y = iy as f64 / (ny-1) as f64;
		[4.0f64 * *units.get_u_lb() * (y-y*y), 0.0f64]
	};
	SetVel2d::new(poiseuille_vel ,&multi_lattice, Box2d::new(0, nx as i32, 0, ny as i32)).process(&mut multi_lattice);

}


fn run(res: usize, max_iter: usize) {
	let res_ref = 10usize;
	let u_ref = 0.05f64;
	let u_max = u_ref * res_ref as f64 / res as f64;
    let units = Converter::new(1.0, 1.0, u_max, res, 200.0);

	let (nx, ny) = units.get_size(2.0, 1.0);

	let (nbx, nby) = load_balancing(num_cpus::get());
	println!("Parallelizing on N = {:?} processors on (nx, ny) = {:?} blocks", num_cpus::get(), (nbx, nby));
    let mut multi_lattice = 
    	multi_lattice2d::MultiLattice2d::new(nx as usize,ny as usize, nbx, nby, 1, BGK::new(units.get_rel_freq()) );

	let mut pf = Vec::<Box<ProcFunMl2d>>::new();

	let b_l = Box2d::new(0,           1,         1,           ny as i32-1);
	let b_r = Box2d::new(nx as i32-1, nx as i32, 1,           ny as i32-1);
	let b_t = Box2d::new(1, nx as i32 - 1,       ny as i32-1, ny as i32);
	let b_b = Box2d::new(1, nx as i32 - 1,       0,           1);

	let b_lt = Box2d::new(0, 1, ny as i32-1, ny as i32);
	let b_lb = Box2d::new(0, 1, 0, 1);
	let b_rb = Box2d::new(nx as i32-1, nx as i32, 0, 1);
	let b_rt = Box2d::new(nx as i32-1, nx as i32, ny as i32-1, ny as i32);


	geom_setup(&mut multi_lattice, &units);
	pf.push(Box::new(FlatInterpDirichlet2d::new(   0, -1, &multi_lattice, b_l)));
	pf.push(Box::new(FlatInterpDirichlet2d::new(   0,  1, &multi_lattice, b_r)));
	pf.push(Box::new(FlatInterpDirichlet2d::new(   1,  1, &multi_lattice, b_t)));
	pf.push(Box::new(FlatInterpDirichlet2d::new(   1, -1, &multi_lattice, b_b)));
	pf.push(Box::new(CornerInterpDirichlet2d::new(-1,  1, &multi_lattice, b_lt)));
	pf.push(Box::new(CornerInterpDirichlet2d::new(-1, -1, &multi_lattice, b_lb)));
	pf.push(Box::new(CornerInterpDirichlet2d::new( 1, -1, &multi_lattice, b_rb)));
	pf.push(Box::new(CornerInterpDirichlet2d::new( 1,  1, &multi_lattice, b_rt)));

	let mut red_pf = Vec::<Box<RedProcFunToScalarMl2d>>::new();
	let error = |f: &[f64; cell::Q], _ix: i32, iy: i32| -> f64 {
		let u = BGK::default().compute_u(f);
		let y = iy as f64 / (ny-1) as f64;
		let u_diff = [u[0] - 4.0f64 * *units.get_u_lb() * (y-y*y), u[1]];
		u_diff[0]*u_diff[0] + u_diff[1]*u_diff[1]
	};

	red_pf.push(Box::new(RedScalarAndPosFun2d::new(error, &multi_lattice, Box2d::new(0, nx as i32, 0, ny as i32))));

	let mut conv = Vec::new();
	conv.push(Converger::new((*units.get_l_lb() as f64 / units.get_u_lb()) as usize, 1.0e-3));

    for i in 0..max_iter {
	    multi_lattice.collide_and_stream();
	    for pfi in pf.iter() {
	    	pfi.process(&mut multi_lattice);
	    }

	    if i%units.get_iter(0.1) == 0 {
		    for (red_pfi, mut convi) in red_pf.iter().zip(conv.iter_mut()) {
	    		convi.take_value((red_pfi.process(&mut multi_lattice) / *units.get_u_lb() / *units.get_u_lb() / Box2d::new(0, nx as i32, 0, ny as i32).size() as f64).sqrt());
		    }
		    if conv.iter().all(|ref x| x.has_converged()) {
		    	let red_pfi = red_pf.pop();
		    	let error = match red_pfi {
					    		Some(red_pfi) => (red_pfi.process(&mut multi_lattice) / *units.get_u_lb() / *units.get_u_lb() / Box2d::new(0, nx as i32, 0, ny as i32).size() as f64).sqrt(),
					    		None => panic!("Error no error can be computed"),
					    	};

		    	println!("Converged with error = {:?}", error);
		    	process::exit(0);
		    }
	    }
	}
	println!("Never converged.");
}

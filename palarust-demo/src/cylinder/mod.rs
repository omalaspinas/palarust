use num_cpus;
use docopt::Docopt;
use palarust::cell::bulk_dyn::BGK;
use palarust::cell::bc_dyn::{BounceBack, VelDirichletDyn, RhoVelDirichletDyn};
use palarust::io::IO;
use palarust::io;
use palarust::cell;
use palarust::moments::Moments;
use palarust::lattice_ops_2d::LatticeOps2d;
use palarust::multi_lattice2d;
use palarust::colormaps;
use palarust::units::Converter;

use palarust::proc_fun::{FlatInterpDirichlet2d,CornerInterpDirichlet2d,ProcFunMl2d,SetDyn2d,SetRho2d,SetVel2d,SetDevStress2d};
use palarust::box2d::Box2d;

const USAGE: &'static str = "
	Simulation of the flow pas a circular cylinder.

	Usage: 
		cylinder [--resolution N --iterations N] 
	    cylinder (-h | --help)


	Options:
	    -h, --help           Show this message.
	    --resolution=<N>     Use a resolution of N in the diameter of the cylinder [default: 10].
	    --iterations=<N>     Simulate for N iterations [default: 10000].
	";

#[derive(RustcDecodable)]
pub struct Args {
    flag_resolution: usize,
    flag_iterations: usize,
}


fn load_balancing(num_cpus: usize) -> (usize, usize) {
	match num_cpus {
		4 => (2,2),
		6 => (3,2),
		8 => (4,2),
		10 => (5,2),
		12 => (4,3),
		14 => (7,2),
		15 => (5,3),
		16 => (4,5),
		_ => (num_cpus,1)
	}
}

pub fn main(args: &[String]) {
    let args: Args =
        Docopt::new(USAGE)
            .and_then(|d| d.argv(args).decode())
            .unwrap_or_else(|e| e.exit());

    run(args.flag_resolution, args.flag_iterations);
}


fn geom_setup(mut multi_lattice: &mut multi_lattice2d::MultiLattice2d, units: & Converter) {
	let (nx, ny) = (multi_lattice.get_nx(), multi_lattice.get_ny());

 	let cx = nx as i32 / 6;
 	let cy = (ny as i32 / 2) + 2;
    let mask = |_x:i32,_y:i32| -> bool { 
		((_x as i32-cx)*(_x as i32-cx)+(_y as i32-cy)*(_y as i32-cy)) < ((units.get_l_lb()/2)*(units.get_l_lb()/2)) as i32
	};

	multi_lattice.set_gen_dyn(BounceBack::new(), mask);

	let b_l = Box2d::new(0,           1,         1,           ny as i32-1);
	let b_r = Box2d::new(nx as i32-1, nx as i32, 1,           ny as i32-1);
	let b_t = Box2d::new(1, nx as i32 - 1,       ny as i32-1, ny as i32);
	let b_b = Box2d::new(1, nx as i32 - 1,       0,           1);

	let b_lt = Box2d::new(0, 1, ny as i32-1, ny as i32);
	let b_lb = Box2d::new(0, 1, 0, 1);
	let b_rb = Box2d::new(nx as i32-1, nx as i32, 0, 1);
	let b_rt = Box2d::new(nx as i32-1, nx as i32, ny as i32-1, ny as i32);


	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0, -1),
		 &multi_lattice, b_l).process(&mut multi_lattice);
	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0,  1), 
		&multi_lattice, b_r).process(&mut multi_lattice);
	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 1,  1),
		 &multi_lattice, b_t).process(&mut multi_lattice);
	SetDyn2d::new(VelDirichletDyn::new(BGK::new(units.get_rel_freq()), 1, -1), 
		&multi_lattice, b_b).process(&mut multi_lattice);

	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0, -1),
		&multi_lattice, b_lt).process(&mut multi_lattice);
	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0, -1), 
		&multi_lattice, b_lb).process(&mut multi_lattice);
	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0,  1),
		&multi_lattice, b_rb).process(&mut multi_lattice);
	SetDyn2d::new(RhoVelDirichletDyn::new(BGK::new(units.get_rel_freq()), 0,  1), 
		&multi_lattice, b_rt).process(&mut multi_lattice);

	let poiseuille_vel = move |_ix: i32, iy:i32| -> [f64; cell::D] {
		let y = iy as f64 / (ny-1) as f64;
		[4.0f64 * *units.get_u_lb() * (y-y*y), 0.0f64]
	};
	SetRho2d::new(|_ix: i32, _iy: i32| -> f64 {1.0} ,&multi_lattice, Box2d::new(0, nx as i32, 0, ny as i32)).process(&mut multi_lattice);
	SetVel2d::new(poiseuille_vel ,&multi_lattice, Box2d::new(0, nx as i32, 0, ny as i32)).process(&mut multi_lattice);
	SetDevStress2d::new(|_ix: i32, _iy: i32| -> [[f64; cell::D]; cell::D] {[[0.0, 0.0], [0.0, 0.0]]}, 
		&multi_lattice, Box2d::new(0, nx as i32, 0, ny as i32)).process(&mut multi_lattice);

}


fn run(res: usize, max_iter: usize) {
    let units = Converter::new(1.0, 1.0, 0.1, res, 200.0);

    let comp_u_norm = |f: &[f64; cell::Q]| -> f64 { 
    	let u = BGK::default().compute_u(&f);
    	(u[0]*u[0]+u[1]*u[1]).sqrt() 
    };

	let (nx, ny) = units.get_size(40.0, 20.0);

 	let mut r= io::init(nx as u32, ny as u32);

	let (nbx, nby) = load_balancing(num_cpus::get());
	println!("Parallelizing on N = {:?} processors on (nx, ny) = {:?} blocks", num_cpus::get(), (nbx, nby));
    let mut multi_lattice = 
    	multi_lattice2d::MultiLattice2d::new(nx as usize,ny as usize, nbx, nby, 1, BGK::new(units.get_rel_freq()) );

	let mut pf = Vec::<Box<ProcFunMl2d>>::new();

	let b_l = Box2d::new(0,           1,         1,           ny as i32-1);
	let b_r = Box2d::new(nx as i32-1, nx as i32, 1,           ny as i32-1);
	let b_t = Box2d::new(1, nx as i32 - 1,       ny as i32-1, ny as i32);
	let b_b = Box2d::new(1, nx as i32 - 1,       0,           1);

	let b_lt = Box2d::new(0, 1, ny as i32-1, ny as i32);
	let b_lb = Box2d::new(0, 1, 0, 1);
	let b_rb = Box2d::new(nx as i32-1, nx as i32, 0, 1);
	let b_rt = Box2d::new(nx as i32-1, nx as i32, ny as i32-1, ny as i32);


	geom_setup(&mut multi_lattice, &units);
	pf.push(Box::new(FlatInterpDirichlet2d::new(0, -1, &multi_lattice, b_l)));
	pf.push(Box::new(FlatInterpDirichlet2d::new(0,  1, &multi_lattice, b_r)));
	pf.push(Box::new(FlatInterpDirichlet2d::new(1,  1, &multi_lattice, b_t)));
	pf.push(Box::new(FlatInterpDirichlet2d::new(1, -1, &multi_lattice, b_b)));
	pf.push(Box::new(CornerInterpDirichlet2d::new(-1,  1, &multi_lattice, b_lt)));
	pf.push(Box::new(CornerInterpDirichlet2d::new(-1, -1, &multi_lattice, b_lb)));
	pf.push(Box::new(CornerInterpDirichlet2d::new( 1, -1, &multi_lattice, b_rb)));
	pf.push(Box::new(CornerInterpDirichlet2d::new( 1,  1, &multi_lattice, b_rt)));

    for i in 0..max_iter {
	    multi_lattice.collide_and_stream();
	    for pfi in pf.iter() {
	    	pfi.process(&mut multi_lattice);
	    }

	    if i%units.get_iter(1.0/24.0) == 0 {
		    multi_lattice.show_scalar(&mut r, &comp_u_norm, &colormaps::ShortRainbow);
	    }

	}
}
